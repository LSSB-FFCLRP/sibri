package br.usp.ffclrp.dcm.lssb.sibri.core;

import java.util.List;
import java.util.Map.Entry;

import br.usp.ffclrp.dcm.lssb.sibri.parameter.AbstractRequiredParameter;
import br.usp.ffclrp.dcm.lssb.sibri.parameter.RequiredParameter;

import java.util.Set;

/**
 * Class to support operation gateway points in service tasks.
 * 
 * @author Wilson Daniel da Silva
 */
public class GatewayServiceTask extends AbstractServiceTask {

	/**
	 * Invoke user parameter interaction logic.
	 */
	@Override
	protected void invoke() throws Exception {

		if (mapOfRequiredParameters.isEmpty()) {
			continueProcessing();

		} else {
			final RequiredParameter requiredParameter = getRequiredParameter();
			final List<AbstractRequiredParameter> requiredParameters = requiredParameter.getRequiredParameter(getServiceOperationId());
			if (requiredParameters.isEmpty()) {
				final Set<Entry<String, String>> entrySet = mapOfRequiredParameters.entrySet();
				for (final Entry<String, String> param : entrySet) {
					buildInputRequiredParameter(param.getKey(), param.getValue(), mapOfDefaultValues.get(param.getKey()));
				}
				stopProcessing();

			} else if (!requiredParameter.getNotAnsweredRequiredParameters().isEmpty()) {
				stopProcessing();

			} else {
				final List<AbstractRequiredParameter> answeredRequiredParameters = requiredParameter.getRequiredParameter(getServiceOperationId());
				for (final AbstractRequiredParameter requiredParameterItem : answeredRequiredParameters) {
					setVariable(requiredParameterItem.getNameOfParameter(), requiredParameterItem.getValueSelected());
				}

				continueProcessing();
			}
		}
	}
}
