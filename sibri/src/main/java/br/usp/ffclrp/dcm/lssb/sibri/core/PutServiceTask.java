package br.usp.ffclrp.dcm.lssb.sibri.core;

import com.sun.jersey.api.client.WebResource;

/**s
 * Invoke web service using the method HTTP PUT.
 * 
 * @author Wilson Daniel da Silva
 */
public class PutServiceTask extends DefaultServiceTask {

	/**
	 * Invoke a web service.
	 */
	protected void invokeService() {
		final String urlPath = getServiceOperationId() + getPathParam();
		final Object bodyData = getBodyData();
		
		final WebResource webResource = getWebResource().path(urlPath);
		if (bodyData == null) {
			webResource.put();
		} else {
			webResource.put(bodyData);
		}
	}
}
