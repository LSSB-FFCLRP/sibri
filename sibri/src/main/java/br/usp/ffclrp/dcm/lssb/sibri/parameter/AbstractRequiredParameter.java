package br.usp.ffclrp.dcm.lssb.sibri.parameter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Abstract class to support requests of parameter to application user.
 * 
 * @author Wilson Daniel da Silva
 */
public abstract class AbstractRequiredParameter implements Serializable {

	/**
	 * Class version.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Name of parameter.
	 */
	private final String nameOfParameter;
	
	/**
	 * Prompt for user.
	 */
	private final String promptForUser;
	
	/**
	 * List of options.
	 */
	private final Map<String, Object> listOfOptions;
	
	/**
	 * Component type.
	 */
	private final ComponentType componentType;
	
	/**
	 * Constructor.
	 * 
	 * @param promptForUser
	 *            Prompt for user.
	 * @param componentType
	 *            Component type.
	 */
	public AbstractRequiredParameter(final String promptForUser, final ComponentType componentType) {
		this(promptForUser, promptForUser, Collections.<String, Object>emptyMap(), componentType);
	}
	
	/**
	 * Constructor.
	 * 
	 * @param promptForUser
	 *            Prompt for user.
	 * @param listOfOptions
	 *            List of options.
	 * @param componentType
	 *            Component type.
	 */
	public AbstractRequiredParameter(final String promptForUser, final Map<String, Object> listOfOptions,
			final ComponentType componentType) {
		this(promptForUser, promptForUser, listOfOptions, componentType);
	}
	
	/**
	 * Constructor.
	 * 
	 * @param nameOfParameter
	 *            Name of parameter.
	 * @param promptForUser
	 *            Prompt for user.
	 * @param componentType
	 *            Component type.
	 */
	public AbstractRequiredParameter(final String nameOfParameter, final String promptForUser,
			final ComponentType componentType) {
		this(nameOfParameter, promptForUser, Collections.<String, Object>emptyMap(), componentType);
	}

	/**
	 * Construtor.
	 * 
	 * @param nameOfParameter
	 *            Name of parameter.
	 * @param promptForUser
	 *            Prompt for user.
	 * @param listOfOptions
	 *            List of options.
	 * @param componentType
	 *            Component type.
	 */
	public AbstractRequiredParameter(final String nameOfParameter, final String promptForUser,
			final Map<String, Object> listOfOptions, final ComponentType componentType) {
		super();
		this.nameOfParameter = nameOfParameter;
		this.promptForUser = promptForUser;
		this.listOfOptions = listOfOptions;
		this.componentType = componentType;
	}

	/**
	 * @return Return map of lsit of options.
	 */
	protected Map<String, Object> getMapListOfOptions() {
		return listOfOptions;
	}
	
	/**
	 * @return Return if parameter is answer or not.
	 */
	public abstract Boolean getAnswered();
	
	/**
	 * @return Return real value selected by user.
	 */
	public Object getRealValueSelected() {
		final Object valueSelected = getValueSelected();
		return listOfOptions.get(valueSelected);
	}

	/**
	 * @return Return value selected by user.
	 */
	public abstract Object getValueSelected();
	
	/**
	 * Set value selected by user.
	 * 
	 * @param valueSelected
	 *            A value.
	 */
	public abstract void setValueSelected(Object valueSelected);

	/**
	 * @return Return the name of parameter.
	 */
	public String getNameOfParameter() {
		return nameOfParameter;
	}

	/**
	 * @return Return a prompt for user.
	 */
	public String getPromptForUser() {
		return promptForUser;
	}

	/**
	 * @return Return the list of options.
	 */
	public List<String> getListOfOptions() {
		final List<String> listOfOptions = new ArrayList<>(this.listOfOptions.keySet());
		Collections.sort(listOfOptions);
		return listOfOptions;
	}

	/**
	 * @return Return the component type.
	 */
	public ComponentType getComponentType() {
		return componentType;
	}

	/**
	 * Return {@link String} representation.
	 */
	@Override
	public String toString() {
		return "AbstractRequiredParameter [nameOfParameter=" + nameOfParameter + ", promptForUser=" + promptForUser
				+ ", listOfOptions=" + listOfOptions + ", componentType=" + componentType + "]";
	}
}
