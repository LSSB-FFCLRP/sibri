package br.usp.ffclrp.dcm.lssb.sibri.parameter;

import java.util.HashMap;
import java.util.Map;

/**
 * Class to support boolean requests of parameter to application user.
 * 
 * @author Wilson Daniel da Silva
 */
public class BooleanRequiredParameter extends AbstractRequiredParameter {

	/**
	 * Class version.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Default list of options.
	 */
	private static final Map<String, Object> LIST_OF_OPTIONS = new HashMap<String, Object>();
	static {
		LIST_OF_OPTIONS.put("yes", Boolean.TRUE);
		LIST_OF_OPTIONS.put("no", Boolean.FALSE);
	}
	
	/**
	 * Value selected by user.
	 */
	private Object valueSelected;

	/**
	 * Constructor.
	 * 
	 * @param nameOfParameter
	 *            Name of parameter.
	 * @param promptForUser
	 *            Prompt for user.
	 */
	public BooleanRequiredParameter(final String nameOfParameter, final String promptForUser) {
		super(promptForUser, LIST_OF_OPTIONS, ComponentType.BOOLEAN);
	}

	/**
	 * Return if parameter is answer or not.
	 */
	public Boolean getAnswered() {
		return valueSelected != null;
	}
	
	/**
	 * Return value selected by user.
	 */
	public Object getValueSelected() {
		return valueSelected;
	}

	/**
	 * Set value selected by user.
	 * 
	 * @param valueSelected
	 *            A value.
	 */
	public void setValueSelected(Object valueSelected) {
		this.valueSelected = valueSelected;
	}

	/**
	 * Return {@link String} representation.
	 */
	@Override
	public String toString() {
		return "BooleanRequiredParameter [valueSelected=" + valueSelected + "]";
	}
}
