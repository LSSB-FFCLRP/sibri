package br.usp.ffclrp.dcm.lssb.sibri.parameter;

import java.util.Map;

/**
 * Class to support select requests of parameter to application user.
 * 
 * @author Wilson Daniel da Silva
 */
public class SelectRequiredParameter extends AbstractRequiredParameter {

	/**
	 * Class version.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Value selected by user.
	 */
	private Object valueSelected;

	/**
	 * Constructor.
	 * 
	 * @param nameOfParameter
	 *            Name of parameter.
	 * @param promptForUser
	 *            Prompt for user.
	 * @param listOfOptions
	 *            List of options for the parameter.
	 */
	public SelectRequiredParameter(final String nameOfParameter, final String promptForUser, final Map<String, Object> listOfOptions) {
		super(nameOfParameter, promptForUser, listOfOptions, ComponentType.SELECT);
	}

	/**
	 * Return if parameter is answer or not.
	 */
	public Boolean getAnswered() {
		return valueSelected != null;
	}
	
	/**
	 * Return value selected by user.
	 */
	public Object getValueSelected() {
		return valueSelected;
	}

	/**
	 * Set value selected by user.
	 * 
	 * @param valueSelected
	 *            A value.
	 */
	public void setValueSelected(Object valueSelected) {
		this.valueSelected = valueSelected;
	}

	/**
	 * Return {@link String} representation.
	 */
	@Override
	public String toString() {
		return "SelectRequiredParameter [valueSelected=" + valueSelected + "]";
	}
}
