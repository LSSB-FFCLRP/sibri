package br.usp.ffclrp.dcm.lssb.sibri.parameter;

/**
 * Class to support input requests of parameter to application user.
 * 
 * @author Wilson Daniel da Silva
 */
public class InputRequiredParameter extends AbstractRequiredParameter {

	/**
	 * Class version.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Default value for parameter.
	 */
	private final Object defaultValue;
	
	/**
	 * Value selected by user.
	 */
	private Object valueSelected;

	/**
	 * Constructor.
	 * 
	 * @param serviceOperationId
	 *            A service operation identifier.
	 * @param nameOfParameter
	 *            Name of parameter.
	 * @param promptForUser
	 *            Prompt for user.
	 * @param defaultValue
	 *            Parameter default value.
	 */
	public InputRequiredParameter(final String nameOfParameter, final String promptForUser, final Object defaultValue) {
		super(nameOfParameter, promptForUser, ComponentType.INPUT);
		this.defaultValue = defaultValue;
	}
	
	/**
	 * Return if parameter is answer or not.
	 */
	public Boolean getAnswered() {
		return valueSelected != null;
	}
	
	/**
	 * Return value selected by user.
	 */
	public Object getValueSelected() {
		return valueSelected;
	}
	
	/**
	 * Return real value selected by user.
	 */
	public Object getRealValueSelected() {
		return getValueSelected();
	}

	/**
	 * Set value selected by user.
	 * 
	 * @param valueSelected
	 *            A value.
	 */
	public void setValueSelected(Object valueSelected) {
		this.valueSelected = valueSelected;
	}

	/**
	 * @return Return default value for parameter.
	 */
	public Object getDefaultValue() {
		return defaultValue;
	}

	/**
	 * Return {@link String} representation.
	 */
	@Override
	public String toString() {
		return "InputRequiredParameter [valueSelected=" + valueSelected + "]";
	}
}
