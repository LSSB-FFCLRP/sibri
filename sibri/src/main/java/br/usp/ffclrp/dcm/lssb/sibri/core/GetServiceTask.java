package br.usp.ffclrp.dcm.lssb.sibri.core;

import com.sun.jersey.api.client.WebResource;

/**
 * Invoke web service using the method HTTP GET.
 * 
 * @author Wilson Daniel da Silva
 */
public class GetServiceTask extends DefaultServiceTask {

	/**
	 * Invoke a web service.
	 */
	protected void invokeService() {
		final String urlPath = getServiceOperationId() + getPathParam();
		final WebResource webResource = getWebResource().path(getServiceOperationId() + urlPath);
		webResource.get(String.class);
	}
}
