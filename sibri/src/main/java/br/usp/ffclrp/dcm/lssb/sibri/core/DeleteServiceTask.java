package br.usp.ffclrp.dcm.lssb.sibri.core;

import com.sun.jersey.api.client.WebResource;

/**
 * Invoke web service using the method HTTP DELETE.
 * 
 * @author Wilson Daniel da Silva
 */
public class DeleteServiceTask extends DefaultServiceTask {

	/**
	 * Invoke a web service.
	 */
	protected void invokeService() {
		final String urlPath = getServiceOperationId() + getPathParam();
		final Object bodyData = getBodyData();
		
		final WebResource webResource = getWebResource().path(urlPath);
		if (bodyData == null) {
			webResource.delete();
		} else {
			webResource.delete(bodyData);
		}
	}
}
