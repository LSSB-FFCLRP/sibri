package br.usp.ffclrp.dcm.lssb.sibri.parameter;

import java.util.Map;

/**
 * Class to support multiple select requests of parameter to application user.
 * 
 * @author Wilson Daniel da Silva
 */
public class MultipleSelectRequiredParameter extends AbstractRequiredParameter {

	/**
	 * Class version.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Values selected by user.
	 */
	private Object[] valueSelected;

	/**
	 * Constructor.
	 * 
	 * @param nameOfParameter
	 *            Name of parameter.
	 * @param promptForUser
	 *            Prompt for user.
	 * @param listOfOptions
	 *            List of options for the parameter.
	 */
	public MultipleSelectRequiredParameter(final String nameOfParameter, final String promptForUser, final Map<String, Object> listOfOptions) {
		super(nameOfParameter, promptForUser, listOfOptions, ComponentType.MULTIPLE);
	}

	/**
	 * Return if parameter is answer or not.
	 */
	public Boolean getAnswered() {
		return valueSelected != null && valueSelected.length != 0;
	}
	
	/**
	 * Return value selected by user.
	 */
	@Override
	public Object[] getValueSelected() {
		return valueSelected;
	}

	/**
	 * Return real value selected by user.
	 */
	@Override
	public Object[] getRealValueSelected() {
		final Map<String, Object> mapListOfOptions = getMapListOfOptions();
		final Object[] newValueSelected;
		if (valueSelected != null) {
			newValueSelected = new Object[valueSelected.length];
			for (int i = 0; i < valueSelected.length; i++) {
				final Object item = valueSelected[i];
				final Object value = mapListOfOptions.get(item);
				newValueSelected[i] = value;
			}
		} else {
			newValueSelected = null;
		}
		return newValueSelected;
	}
	
	/**
	 * Set value selected by user.
	 * 
	 * @param valueSelected
	 *            A value.
	 */
	@Override
	public void setValueSelected(Object valueSelected) {
		this.valueSelected = (Object[]) valueSelected;
	}
	
	/**
	 * Set value selected by user.
	 * 
	 * @param valueSelected
	 *            A value.
	 */
	public void setValueSelected(Object[] valueSelected) {
		this.valueSelected = valueSelected;
	}

	/**
	 * Return {@link String} representation.
	 */
	@Override
	public String toString() {
		return "MultipleSelectRequiredParameter [valueSelected=" + valueSelected + "]";
	}
}
