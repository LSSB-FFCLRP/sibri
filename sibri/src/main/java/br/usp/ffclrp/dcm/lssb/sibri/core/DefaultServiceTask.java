package br.usp.ffclrp.dcm.lssb.sibri.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ws.rs.HttpMethod;

import org.apache.commons.lang3.StringUtils;

import com.sun.jersey.api.client.WebResource;

import br.usp.ffclrp.dcm.lssb.sibri.parameter.AbstractRequiredParameter;
import br.usp.ffclrp.dcm.lssb.sibri.parameter.RequiredParameter;

/**
 * Generic implementation for {@link AbstractServiceTask}. 
 * 
 * <p>This class allows to
 * support GET, POST, PUT and DELETE operations in a simple way.
 * 
 * @author Wilson Daniel da Silva
 */
public class DefaultServiceTask extends AbstractServiceTask {

	private static final String IDENTIFIER = "identifier";
	private Map<String, Object> mapOfObjects;
	final String BLANK_PATH = "";
	
	/**
	 * Generic implementation to invoke web services following the interaction
	 * rules with user.
	 */
	@Override
	protected void invoke() throws Exception {
		initialize();

		if (mapOfRequiredParameters.isEmpty()) {
			invokeService();

		} else {
			final RequiredParameter requiredParameter = getRequiredParameter();
			List<AbstractRequiredParameter> requiredParameters = requiredParameter.getRequiredParameter(getServiceOperationId());
			if (requiredParameters.isEmpty()) {
				Set<Entry<String, String>> entrySet = mapOfRequiredParameters.entrySet();
				for (final Entry<String, String> param : entrySet) {
  					buildInputRequiredParameter(param.getKey(), param.getValue(), mapOfDefaultValues.get(param.getKey()));
				}
  				stopProcessing();
  				
  			} else  if (!requiredParameter.getNotAnsweredRequiredParameters().isEmpty()) {
  				stopProcessing();
  				
  			} else {
  				final List<AbstractRequiredParameter> answeredRequiredParameters = requiredParameter.getRequiredParameter(getServiceOperationId());
  				for (final AbstractRequiredParameter requiredParameterItem : answeredRequiredParameters) {
					setVariable(requiredParameterItem.getNameOfParameter(), requiredParameterItem.getValueSelected());
					final Object objectAux = builderParam(requiredParameterItem.getNameOfParameter(), requiredParameterItem.getValueSelected());
					if (objectAux == null) {
						mapOfObjects.put(requiredParameterItem.getNameOfParameter(), requiredParameterItem.getValueSelected());
					} else {
						mapOfObjects.put(requiredParameterItem.getNameOfParameter(), objectAux);
					}
				}
				invokeService();
				continueProcessing();
			}
		}
	}

	/**
	 * Initialize map of objects.
	 */
	private void initialize() {
		mapOfObjects = new HashMap<>();
		mapOfObjects.put(IDENTIFIER, getSessionId());
	}

	/**
	 * Invoke service using correct HTTP method.
	 */
	protected void invokeService() {
		
		final String urlPath = getServiceOperationId() + getPathParam();
		final Object bodyData = getBodyData();
		final String httpMethod = getHttpMethod();
		
		final WebResource webResource = getWebResource().path(getServiceOperationId() + urlPath);
		
		if (HttpMethod.GET.equals(httpMethod)) {
			webResource.get(String.class);
					
		} else if (HttpMethod.POST.equals(httpMethod)) {
			if (bodyData == null) {
				webResource.post();
			} else {
				webResource.post(bodyData);
			}
			
		} else if (HttpMethod.PUT.equals(httpMethod)) {
			if (bodyData == null) {
				webResource.put();
			} else {
				webResource.put(bodyData);
			}
			
		} else if (HttpMethod.DELETE.equals(httpMethod)) {
			if (bodyData == null) {
				webResource.delete();
			} else {
				webResource.delete(bodyData);
			}
		}
	}

	/**
	 * @return Return the pattern of URL following rules of BPMN mapping.
	 */
	protected String getPathParam() {
		String pathParam;
		if (StringUtils.isBlank(valueOfUrlPathParam)) {
			pathParam = BLANK_PATH;
		} else {
			pathParam = valueOfUrlPathParam;
			
			// Removes the session identifier to prevent problems with parameter
			// names that have as part of the name the word identifier. Later it
			// is put back on the map.
			final Object sessionIdentifierRemoved = mapOfObjects.remove(IDENTIFIER);
			
			final Set<Entry<String, Object>> entrySet = mapOfObjects.entrySet();
			for (final Entry<String, Object> entry : entrySet) {
				pathParam = pathParam.replace(entry.getKey(), entry.getValue().toString());
			}	
			
			// Re-adds the identifier
			mapOfObjects.put(IDENTIFIER, sessionIdentifierRemoved);
			// Last change the session identifier
			pathParam = pathParam.replace(IDENTIFIER, sessionIdentifierRemoved.toString());
		}
		return pathParam;
	}
	

	/**
	 * @return Return object that will be used on HTTP request body.
	 */
	protected Object getBodyData() {
		final Object bodyData;
		if (StringUtils.isBlank(valueOfBodyData)) {
			bodyData = null;
		} else {
			final String bodyDataStr = valueOfBodyData;
			final String[] objsOrder = bodyDataStr.split(",");
			final List<Object> listOfObjects = new ArrayList<>();
			for (final String obj : objsOrder) {
				final Object objectInMap = mapOfObjects.get(obj);
				if (objectInMap != null) {
					listOfObjects.add(objectInMap);
				}
			}
			bodyData = listOfObjects.size() > 1 ? listOfObjects.toArray() : listOfObjects.get(0);
		}
		return bodyData;
	}
	
	/**
	 * @return Return HTTP method. If none has been defined, the HTTP GET is
	 *         return.
	 */
	private String getHttpMethod() {
		final String httpMethod;
		if (valueOfHttpMethod == null) {
			httpMethod = HttpMethod.GET; // defaultMethod
		} else {
			httpMethod = valueOfBodyData;
		}
		return httpMethod;
	}
}
