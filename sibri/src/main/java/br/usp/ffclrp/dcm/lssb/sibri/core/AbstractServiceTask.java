package br.usp.ffclrp.dcm.lssb.sibri.core;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.HttpMethod;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.delegate.JavaDelegate;


import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import br.usp.ffclrp.dcm.lssb.sibri.builder.ObjectBuilder;
import br.usp.ffclrp.dcm.lssb.sibri.parameter.AbstractRequiredParameter;
import br.usp.ffclrp.dcm.lssb.sibri.parameter.RequiredParameter;

/**
 * Abstract class containing operations common to classes that implement
 * {@code JavaServiceTasks}.
 * 
 * @author Wilson Daniel da Silva
 */
public abstract class AbstractServiceTask implements JavaDelegate {

	// Messages
	private static final String PARAMETER_CLIENT_NULL = "Value client from AbstractJavaServiceTask did not defined.";
	private static final String PARAMETER_EXECUTION_NULL = "Value execution from AbstractJavaServiceTask did not defined.";

	// Constants used on web services processing.
	private static final String WSDL_ADDRESS = "wsdl:address";
	private static final String IDENTIFIER = "identifier";
	private static final String NEXT_STEP = "nextStep";
	private static final String NO = "no";
	private static final String YES = "yes";

	/**
	 * Activiti delegate execution.
	 */
	private DelegateExecution execution;
	
	/**
	 * Webservices client.
	 */
	private Client client;

	/**
	 * Sets the requested parameters in a mandatory way during the execution of
	 * an operation. For example: x,y:type,z,...
	 */
	private Expression requiredParameters;
	
	/**
	 * Sets optional default values for the parameters defined by the
	 * requiredParameters attribute. For example: x:1,y:2,z:0,...
	 */
	private Expression defaultValues;
	
	/**
	 * Specify a Java class responsible for convert value parameters into a
	 * object in a specific format before task invocation. For example:
	 * x:builder,y:builder,...
	 */
	private Expression builders;
	
	/**
	 * Defines the HTTP method that must be invoked for the execution of the
	 * operation. For example: POST.
	 */
	private Expression httpMethod;
	
	/**
	 * Defines the URL structure used to pass parameters in invocation of the
	 * operation. For example: /identifier/source/target.
	 */
	private Expression urlPathParam;
	
	/**
	 * Indicates the existence of a parameter that must be sent in the body of
	 * the HTTP request. For example: obj1,obj2,obj3.
	 */
	private Expression bodyData;

	// Auxiliary fields to control state of this class.
	protected Map<String, String> mapOfRequiredParameters;
	protected Map<String, Object> mapOfDefaultValues;
	private Map<String, Class<ObjectBuilder>> mapOfBuilders;
	protected String valueOfHttpMethod;
	protected String valueOfUrlPathParam;
	protected String valueOfBodyData;

	/**
	 * Perform tasks to process logic of service.
	 */
	@Override
	public void execute(final DelegateExecution execution) throws Exception {
		initialize(execution);
		invoke();
	}

	/**
	 * Invoke the service logic implemented by concrete class.
	 * 
	 * @throws Exception
	 *             If occurred an error during processing.
	 */
	protected abstract void invoke() throws Exception;

	/**
	 * Initialize fields that control state of this class.
	 * 
	 * @param execution
	 *            Activiti delegate execution object.
	 * @throws Exception
	 *             If occurred an error during processing.
	 */
	private void initialize(final DelegateExecution execution) throws Exception {
		this.execution = execution;
		this.client = Client.create(new DefaultClientConfig());
		initializeMapOfRequiredParameters();
		initializeMapOfDefaultValues();
		initializeMapOfBuilders();
		initializeValueOfHttpMethod();
		initializeValueOfUrlPathParam();
		initializeValueOfBodyData();
	}

	/**
	 * Initialize map of required parameters.
	 */
	private void initializeMapOfRequiredParameters() {
		mapOfRequiredParameters = new LinkedHashMap<>();
		if (requiredParameters != null) {
			final String params = requiredParameters.getExpressionText();
			String[] split = params.split(",");
			for (String param : split) {
				String[] split2 = param.split(":");
				if (split2.length > 1) {
					mapOfRequiredParameters.put(split2[0], split2[1]);
				} else {
					mapOfRequiredParameters.put(split2[0], split2[0]);
				}
			}
		}
	}

	/**
	 * Initialize map of default values for required parameters.
	 */
	private void initializeMapOfDefaultValues() {
		mapOfDefaultValues = new HashMap<>();
		if (defaultValues != null) {
			final String params = defaultValues.getExpressionText();
			String[] split = params.split(",");
			for (String param : split) {
				String[] split2 = param.split(":");
				if (split2.length > 1) {
					mapOfDefaultValues.put(split2[0], split2[1]);
				} else {
					mapOfDefaultValues.put(split2[0], split2[0]);
				}
			}
		}
	}

	/**
	 * Initialize map of builders for required parameters.
	 * 
	 * @throws ClassNotFoundException
	 *             If an error occurred during builder instance initialization.
	 */
	@SuppressWarnings("unchecked")
	private void initializeMapOfBuilders() throws ClassNotFoundException {
		mapOfBuilders = new HashMap<String, Class<ObjectBuilder>>();
		if (builders != null) {
			final String params = builders.getExpressionText();
			String[] split = params.split(",");
			for (String param : split) {
				String[] builder = param.split(":");
				Class<?> clazz = Class.forName(builder[1]);
				mapOfBuilders.put(builder[0], (Class<ObjectBuilder>) clazz);
			}
		}
	}

	/**
	 * Initialize value of HTTP method.
	 */
	private void initializeValueOfHttpMethod() {
		valueOfHttpMethod = null;
		if (httpMethod != null) {
			final String httpMethod = this.httpMethod.getExpressionText();
			switch (httpMethod) {
			case HttpMethod.GET:
			case HttpMethod.POST:
			case HttpMethod.PUT:
			case HttpMethod.DELETE:
				valueOfHttpMethod = httpMethod;
				break;
			default:
				throw new IllegalArgumentException("HTTP method invalid or unsupported.");
			}
		}
	}

	/**
	 * Initialize value of url path param.
	 */
	private void initializeValueOfUrlPathParam() {
		valueOfUrlPathParam = null;
		if (urlPathParam != null) {
			valueOfUrlPathParam = this.urlPathParam.getExpressionText();
		}
	}

	/**
	 * Initialize value of body data.
	 */
	private void initializeValueOfBodyData() {
		valueOfBodyData = null;
		if (bodyData != null) {
			valueOfBodyData = this.bodyData.getExpressionText();
		}
	}

	/**
	 * Validate required parameters to initialize this instance.
	 */
	private void validate() {
		if (execution == null) {
			throw new IllegalStateException(PARAMETER_EXECUTION_NULL);
		}

		if (client == null) {
			throw new IllegalStateException(PARAMETER_CLIENT_NULL);
		}
	}

	/**
	 * @return Return web service resource reference.
	 */
	protected WebResource getWebResource() {
		return getClient().resource((String) execution.getVariable(WSDL_ADDRESS));
	}

	/**
	 * @return Return service operation ID.
	 */
	protected String getServiceOperationId() {
		return getExecution().getCurrentActivityId();
	}

	/**
	 * @return Return session ID.
	 */
	protected String getSessionId() {
		return (String) getExecution().getVariable(IDENTIFIER);
	}

	/**
	 * @return Return Activiti delegate execution object instance.
	 */
	protected DelegateExecution getExecution() {
		validate();
		return execution;
	}

	/**
	 * @return Return instance of client web services.
	 */
	private Client getClient() {
		validate();
		return client;
	}

	/**
	 * Return a value of BPMN variable cast to String.
	 * 
	 * @param name
	 *            A name of BPMN variable.
	 * @return Return a value of variable cast to String.
	 */
	protected String getVariableAsString(final String name) {
		return (String) getVariable(name);
	}

	/**
	 * Return a value of BPMN variable.
	 * 
	 * @param name
	 *            A name of BPMN variable.
	 * @return Return a value of variable.
	 */
	protected Object getVariable(final String name) {
		return getExecution().getVariable(name);
	}

	/**
	 * Set a BPMN value.
	 * 
	 * @param name
	 *            A name of BPMN variable.
	 * @param value
	 *            A value to set.
	 */
	protected void setVariable(final String name, final Object value) {
		getExecution().setVariable(name, value);
	}

	/**
	 * @return Return a required parameter.
	 */
	protected RequiredParameter getRequiredParameter() {
		RequiredParameter requiredParameter = (RequiredParameter) getVariable(RequiredParameter.NAME);
		if (requiredParameter == null) {
			requiredParameter = new RequiredParameter();
			updateRequiredParameter(requiredParameter);
		}
		return requiredParameter;
	}

	/**
	 * Update a required parameter defined as BPMN variable by instance passed
	 * with parameter.
	 * 
	 * @param requiredParameter
	 *            A required parameter.
	 */
	private void updateRequiredParameter(final RequiredParameter requiredParameter) {
		setVariable(RequiredParameter.NAME, requiredParameter);
	}

	/**
	 * Build an input required parameter.
	 * 
	 * @param nameOfParameter
	 *            Name of parameter.
	 * @param promptForUser
	 *            Prompt for user.
	 * @param defaultValue
	 *            Parameter default value.
	 */
	protected void buildInputRequiredParameter(final String nameOfParameter, final String promptForUser,
			final Object defaultValue) {
		final RequiredParameter requiredParameter = getRequiredParameter();
		requiredParameter.buildInputRequiredParameter(getServiceOperationId(), nameOfParameter, promptForUser, defaultValue);
		updateRequiredParameter(requiredParameter);
	}

	/**
	 * Build a select required parameter.
	 * 
	 * @param nameOfParameter
	 *            Name of parameter.
	 * @param promptForUser
	 *            Prompt for user.
	 * @param listOfOptions
	 *            List of options for the parameter.
	 */
	protected void buildSelectRequiredParameter(final String nameOfParameter, final String promptForUser,
			final Map<String, Object> listOfOptions) {
		final RequiredParameter requiredParameter = getRequiredParameter();
		requiredParameter.buildSelectRequiredParameter(getServiceOperationId(), nameOfParameter, promptForUser, listOfOptions);
		updateRequiredParameter(requiredParameter);
	}

	/**
	 * Build a multiple selected required parameter.
	 * 
	 * @param nameOfParameter
	 *            Name of parameter.
	 * @param promptForUser
	 *            Prompt for user.
	 * @param listOfOptions
	 *            List of options for the parameter.
	 */
	protected void buildMultipleSelectRequiredParameter(final String nameOfParameter, final String promptForUser,
			final Map<String, Object> listOfOptions) {
		final RequiredParameter requiredParameter = getRequiredParameter();
		requiredParameter.buildMultipleSelectRequiredParameter(getServiceOperationId(), nameOfParameter, promptForUser, listOfOptions);
		updateRequiredParameter(requiredParameter);
	}

	/**
	 * Build a boolean required parameter.
	 * 
	 * @param nameOfParameter
	 *            Name of parameter.
	 * @param promptForUser
	 *            Prompt for user.
	 */
	protected void buildBooleanRequiredParameter(final String nameOfParameter, final String promptForUser) {
		final RequiredParameter requiredParameter = getRequiredParameter();
		requiredParameter.buildBooleanRequiredParameter(getServiceOperationId(), nameOfParameter, promptForUser);
		updateRequiredParameter(requiredParameter);
	}

	/**
	 * Method to invoke builder at {@link ObjectBuilder} interface to build
	 * objects.
	 * 
	 * @param nameOfParameter
	 *            Name of parameter.
	 * @param value
	 *            A value used to build object.
	 * @return Return the object built.
	 * @throws Exception
	 *             If occurred an error during processing.
	 */
	protected Object builderParam(String nameOfParameter, Object value) throws Exception {
		final Class<ObjectBuilder> clazz = mapOfBuilders.get(nameOfParameter);
		if (clazz == null) {
			return null;
		} else {
			final ObjectBuilder objectBuilder = clazz.newInstance();
			return objectBuilder.build(value);
		}
	}

	/**
	 * Return whether exist a required parameter not answered.
	 * 
	 * @param requiredParameters
	 *            List of required parameters.
	 * @return Return whether exist or not.
	 */
	protected boolean getExistNotAnsweredRequiredParameter(final List<AbstractRequiredParameter> requiredParameters) {
		for (AbstractRequiredParameter abstractRequiredParameter : requiredParameters) {
			if (!abstractRequiredParameter.getAnswered()) {
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}	
	
	/**
	 * @return Return execution ID.
	 */
	protected String getId() {
		return getExecution().getId();
	}

	/**
	 * Invoke a stop processing.
	 */
	protected void stopProcessing() {
		setVariable(NEXT_STEP, NO);
	}

	/**
	 * Invoke a continue processing.
	 */
	protected void continueProcessing() {
		setVariable(NEXT_STEP, YES);
	}

	/**
	 * Set next step to BPMN flow.
	 * 
	 * @param value
	 *            A value to the next step.
	 */
	protected void setNextStep(final String value) {
		setVariable(NEXT_STEP, value);
	}
}
