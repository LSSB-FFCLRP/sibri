package br.usp.ffclrp.dcm.lssb.sibri.core;


/**
 * Class to represent a Java Service Task that not implement yet.
 * 
 * @author Wilson Daniel da Silva
 */
public class ServiceTaskNotImplemented extends AbstractServiceTask {
	
	/** Does nothing. */
	@Override public void invoke() throws Exception { }
}
