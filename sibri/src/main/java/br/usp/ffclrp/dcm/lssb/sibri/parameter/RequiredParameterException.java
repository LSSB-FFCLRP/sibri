package br.usp.ffclrp.dcm.lssb.sibri.parameter;

import javax.ejb.ApplicationException;

/**
 * Exception throwed when a {@link RequiredParameter} is necessary to continue
 * processing.
 * 
 * @author Wilson Daniel da Silva
 */
@ApplicationException(rollback = false)
public class RequiredParameterException extends Exception {

	/**
	 * Class version.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Default message.
	 */
	private static final String DEFAULT_MESSAGE = "Please enter the required information";

	/**
	 * The required parameter that has not been answered and needs to be
	 * requested from the user.
	 */
	private final AbstractRequiredParameter requiredParameter;

	/**
	 * Constructor.
	 * 
	 * @param message
	 *            A message.
	 * @param requiredParameter
	 *            The required parameter.
	 */
	public RequiredParameterException(final String message, final AbstractRequiredParameter requiredParameter) {
		super(DEFAULT_MESSAGE);
		this.requiredParameter = requiredParameter;
	}

	/**
	 * Constructor.
	 * 
	 * @param requiredParameter
	 *            The required parameter.
	 */
	public RequiredParameterException(final AbstractRequiredParameter requiredParameter) {
		this(DEFAULT_MESSAGE, requiredParameter);
	}

	/**
	 * @return Return required parameter.
	 */
	public AbstractRequiredParameter getRequiredParameter() {
		return requiredParameter;
	}
}
