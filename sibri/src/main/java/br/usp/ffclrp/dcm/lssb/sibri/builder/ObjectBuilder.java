package br.usp.ffclrp.dcm.lssb.sibri.builder;

/**
 * Interface to enable a builder behavior to a class that needs to construct
 * objects to send to web services.
 * 
 * <p>
 * This class can be used to change data format before to send to web services.
 * 
 * @author Wilson Daniel da Silva
 */
public interface ObjectBuilder {

	/**
	 * Method to build an alternative representation to an object.
	 * 
	 * @param value
	 *            Source representation.
	 * @return Return a new target representation.
	 * @throws Exception
	 *             If occurred an error during processing.
	 */
	Object build(Object value) throws Exception;
}
