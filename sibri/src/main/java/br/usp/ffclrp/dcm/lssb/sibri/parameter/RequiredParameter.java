package br.usp.ffclrp.dcm.lssb.sibri.parameter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

/**
 * Class to manipulate user required parameters.
 * 
 * @author Wilson Daniel da Silva
 */
public class RequiredParameter implements Serializable {

	/**
	 * Class version.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Name used to recover this parameter. 
	 */
	public static final String NAME = "p_RequiredParameter";
	
	/**
	 * Parameters in this instance of required parameter.
	 */
	private Map<String, List<AbstractRequiredParameter>>  parameters;
	
	/**
	 * Constructor.
	 */
	public RequiredParameter() {
		parameters = new TreeMap<>();
	}

	/**
	 * Build an input required parameter.
	 * 
	 * @param serviceOperationId
	 *            A service operation identifier.
	 * @param nameOfParameter
	 *            Name of parameter.
	 * @param promptForUser
	 *            Prompt for user.
	 * @param defaultValue
	 *            Parameter default value.
	 */
	public void buildInputRequiredParameter(final String serviceOperationId, final String nameOfParameter, final String promptForUser, final Object defaultValue) {
		final InputRequiredParameter requiredParameter = new InputRequiredParameter(nameOfParameter, promptForUser, defaultValue);
		addRequiredParameter(serviceOperationId, requiredParameter);
	}
	
	/**
	 * Build a select required parameter.
	 * 
	 * @param serviceOperationId
	 *            A service operation identifier.
	 * @param nameOfParameter
	 *            Name of parameter.
	 * @param promptForUser
	 *            Prompt for user.
	 * @param listOfOptions
	 *            List of options for the parameter.
	 */
	public void buildSelectRequiredParameter(final String serviceOperationId, final String nameOfParameter, final String promptForUser, final Map<String, Object> listOfOptions) {
		final SelectRequiredParameter requiredParameter = new SelectRequiredParameter(nameOfParameter, promptForUser, listOfOptions);
		addRequiredParameter(serviceOperationId, requiredParameter);
	}
	
	/**
	 * Build a multiple selected required parameter.
	 * 
	 * @param serviceOperationId
	 *            A service operation identifier.
	 * @param nameOfParameter
	 *            Name of parameter.
	 * @param promptForUser
	 *            Prompt for user.
	 * @param listOfOptions
	 *            List of options for the parameter.
	 */
	public void buildMultipleSelectRequiredParameter(final String serviceOperationId, final String nameOfParameter, final String promptForUser, final Map<String, Object> listOfOptions) {
		final MultipleSelectRequiredParameter requiredParameter = new MultipleSelectRequiredParameter(nameOfParameter, promptForUser, listOfOptions);
		addRequiredParameter(serviceOperationId, requiredParameter);
	}
	
	/**
	 * Build a boolean required parameter.
	 * 
	 * @param serviceOperationId
	 *            A service operation identifier.
	 * @param nameOfParameter
	 *            Name of parameter.
	 * @param promptForUser
	 *            Prompt for user.
	 */
	public void buildBooleanRequiredParameter(final String serviceOperationId, final String nameOfParameter, final String promptForUser) {
		final BooleanRequiredParameter requiredParameter = new BooleanRequiredParameter(nameOfParameter, promptForUser);
		addRequiredParameter(serviceOperationId, requiredParameter);
	}

	/**
	 * Add required parameter to list of parameters.
	 * 
	 * @param serviceOperationId
	 *            Service operation ID.
	 * @param requiredParameter
	 *            Required parameter.
	 */
	private void addRequiredParameter(final String serviceOperationId,
			final AbstractRequiredParameter requiredParameter) {
		
		List<AbstractRequiredParameter> listOfParameters = parameters.get(serviceOperationId);
		if (listOfParameters == null) {
			listOfParameters = new ArrayList<>();
		}
		
		listOfParameters.add(requiredParameter);
		parameters.put(serviceOperationId, listOfParameters);
	}
	
	/**
	 * Return the list of required parameters by service operation
	 * identification.
	 * 
	 * @param serviceOperationId
	 *            service operation identification
	 * @return Return the list.
	 */
	public List<AbstractRequiredParameter> getRequiredParameter(final String serviceOperationId) {
		List<AbstractRequiredParameter> listOfAbstractRequiredParameter = parameters.get(serviceOperationId);
		if (listOfAbstractRequiredParameter == null) {
			listOfAbstractRequiredParameter = new ArrayList<>();
		}
		return listOfAbstractRequiredParameter;
	}
	
	/**
	 * Get first required parameter identified by service operation
	 * identification.
	 * 
	 * @param serviceOperationId
	 *            service operation identification.
	 * @return Return required parameter.
	 */
	public AbstractRequiredParameter getFirstRequiredParameter(final String serviceOperationId) {
		final List<AbstractRequiredParameter> requiredParameter = getRequiredParameter(serviceOperationId);
		if (requiredParameter == null || requiredParameter.isEmpty()) {
			return null;
		} else {
			return requiredParameter.get(0);
		}
	}
	
	/**
	 * @return Returns whether the parameters were answered or not.
	 */
	public Boolean getCompleteAnswered() {
		final Set<Entry<String, List<AbstractRequiredParameter>>> allParameters = parameters.entrySet();
		for (final Entry<String, List<AbstractRequiredParameter>> parameter : allParameters) {
			final List<AbstractRequiredParameter> listOfParameters = parameter.getValue();
			for (final AbstractRequiredParameter requiredParameter : listOfParameters) {
				if (!requiredParameter.getAnswered()) {
					return Boolean.FALSE;
				}
			}
		}
		return Boolean.TRUE;
	}
	
	/**
	 * @return Return all required parameters.
	 */
	public List<AbstractRequiredParameter> getAllRequiredParameters() {
		final List<AbstractRequiredParameter> listOfAllRequiredParameters = new ArrayList<>();
		final Set<Entry<String, List<AbstractRequiredParameter>>> allParameters = parameters.entrySet();
		for (final Entry<String, List<AbstractRequiredParameter>> parameter : allParameters) {
			final List<AbstractRequiredParameter> listOfParameters = parameter.getValue();
			listOfAllRequiredParameters.addAll(listOfParameters);
		}
		return listOfAllRequiredParameters;
	}
	
	/**
	 * @return Return all answered required parameters.
	 */
	public List<AbstractRequiredParameter> getAnsweredRequiredParameters() {
		final List<AbstractRequiredParameter> listOfAnsweredRequiredParameters = new ArrayList<>();
		final List<AbstractRequiredParameter> allRequiredParameters = getAllRequiredParameters();
		for (final AbstractRequiredParameter requiredParameter : allRequiredParameters) {
			if (requiredParameter.getAnswered()) {
				listOfAnsweredRequiredParameters.add(requiredParameter);
			}
		}
		return listOfAnsweredRequiredParameters;
	}
	
	/**
	 * @return Return all unanswered required parameters.
	 */
	public List<AbstractRequiredParameter> getNotAnsweredRequiredParameters() {
		final List<AbstractRequiredParameter> listOfNotAnsweredRequiredParameters = new ArrayList<>();
		final List<AbstractRequiredParameter> allRequiredParameters = getAllRequiredParameters();
		for (final AbstractRequiredParameter requiredParameter : allRequiredParameters) {
			if (!requiredParameter.getAnswered()) {
				listOfNotAnsweredRequiredParameters.add(requiredParameter);
			}
		}
		return listOfNotAnsweredRequiredParameters;
	}

	/**
	 * Return {@link String} representation.
	 */
	@Override
	public String toString() {
		return "RequiredParameter [parameters=" + parameters + "]";
	}
}
