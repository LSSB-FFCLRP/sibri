package br.usp.ffclrp.dcm.lssb.sibri.parameter;

/**
 * Enum to represent component types avaliable to display parameter to
 * application user.
 * 
 * @author Wilson Daniel da Silva
 */
public enum ComponentType {
	SELECT, MULTIPLE, BOOLEAN, INPUT;
}
